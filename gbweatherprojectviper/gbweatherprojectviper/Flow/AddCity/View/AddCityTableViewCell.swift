import UIKit

class AddCityTableViewCell: UITableViewCell {
    
    static let reuseId = "AddCityTableViewCell"
    
    private let cityNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 25, weight: .regular)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    private func configureUI() {
        let horizontalStackView = UIStackView(arrangedSubviews: [
            cityNameLabel
        ])
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 10
        horizontalStackView.alignment = .center
        addSubview(horizontalStackView)
        horizontalStackView.anchor(top: topAnchor,
                                   leading: leadingAnchor,
                                   bottom: bottomAnchor,
                                   trailing: trailingAnchor,
                                   padding: .init(top: 10, left: 20, bottom: 10, right: 20))
    }
    
    func configure(city: CityResponse?) {
        cityNameLabel.text = city?.name
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
