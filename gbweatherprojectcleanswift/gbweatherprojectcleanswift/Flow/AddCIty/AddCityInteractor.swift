import UIKit

protocol AddCityBusinessLogic {
    func fetchCities(searchCity: String)
}

protocol AddCityDataStore {
    var allCities: [CityResponse] { get set }
}

class AddCityInteractor: AddCityBusinessLogic, AddCityDataStore {
    
    var presenter: AddCityPresentationLogic?
    var allCities: [CityResponse] = []
    // MARK: Fetch Cities
    
    func fetchCities(searchCity: String) {
        
        allCities = LoadServiceJSONFromFile.load("cities.json")
    
        self.allCities = self.allCities.filter({ (city) -> Bool in
            return city.name.lowercased().contains(searchCity.lowercased())
        })
        
        let response = AddCity.Something.Response(cities: allCities)
        presenter?.presentCities(response: response)
    }
}
