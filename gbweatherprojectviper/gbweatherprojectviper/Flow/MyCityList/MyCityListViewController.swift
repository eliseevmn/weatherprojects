import UIKit

protocol MyCityListViewProtocol {
    var presenter: MyCityListPresenterProtocol? { get set }
    func reloadData()
}

class MyCityListViewController: UIViewController {

    // MARK: - Outlets
    private var tableView = UITableView(frame: .zero, style: .plain)
    private var refreshControler: CustomRefreshController = {
        let refreshControler = CustomRefreshController(frame: .zero)
        refreshControler.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        return refreshControler
    }()
    private var addCityButton: CustomButton = {
        let button = CustomButton(widthAndHeight: 70, image: #imageLiteral(resourceName: "addCity"))
        button.addTarget(self, action: #selector(handleAddCity), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    var presenter: MyCityListPresenterProtocol?
    private let configurator: MyCityListConfiguratorProtocol = MyCityListConfigurator()
    
    // MARK: - ViewController lyfecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
        presenter?.showCitiesList(cityName: "Moscow")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Private functions
    private func setupUI() {
        tableView.addSubview(refreshControler)
        view.addSubview(tableView)
        tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .black
        
        tableView.register(MyCityListTableViewCell.self, forCellReuseIdentifier: MyCityListTableViewCell.reuseId)
        
        view.addSubview(addCityButton)
        addCityButton.anchor(top: nil,
                             leading: nil,
                             bottom: view.safeAreaLayoutGuide.bottomAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 0, left: 0, bottom: 40, right: 16))
    }
    
    // MARK: - Actions
    @objc private func handleAddCity() {
        presenter?.showAddCityController()
    }
    
    @objc private func handleRefresh(sender: UIRefreshControl) {
        presenter?.refrestWeatherData()
        sender.endRefreshing()
    }
}

// MARK: - UITableViewDataSource
extension MyCityListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.citiesCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyCityListTableViewCell.reuseId,
                                                 for: indexPath) as! MyCityListTableViewCell
        
        presenter?.selectRow(at: indexPath)
        cell.configure(with: presenter?.myCity)
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MyCityListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.selectRow(at: indexPath)
        presenter?.showDetailController()
    }
}

// MARK: - UI Setup
extension MyCityListViewController: MyCityListViewProtocol {
    
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
