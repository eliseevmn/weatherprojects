import UIKit

protocol AddCityPresentationLogic {
    func presentCities(response: AddCity.Something.Response)
}

class AddCityPresenter: AddCityPresentationLogic {
    
    weak var viewController: AddCityDisplayLogic?
    var worker: AddCityWorker?
    
    // MARK: - Present Cities
    
    func presentCities(response: AddCity.Something.Response) {
        worker = AddCityWorker()
        
        guard let displayedCities = worker?.getDisplayedCities(from: response.cities) else { return }
        
        let viewModel = AddCity.Something.ViewModel(displayedSearchCities: displayedCities)
        viewController?.displayCIties(viewModel: viewModel)
    }
}
