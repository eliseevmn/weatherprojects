import SwiftUI

struct CurrentWeatherView: View {

    // MARK: - Proprties
    @ObservedObject var viewModel: CurrentWeatherViewModel

    // MARK: - Init
    init(viewModel: CurrentWeatherViewModel) {
        self.viewModel = viewModel
    }

    // MARK: - Body
    var body: some View {
        List(content: content)
            .onAppear(perform: viewModel.refresh)
            .navigationBarTitle(viewModel.city)
            .listStyle(GroupedListStyle())
    } // Body
}

private extension CurrentWeatherView {
    func content() -> some View {
        if let viewModel = viewModel.dataSource {
            return AnyView(details(for: viewModel))
        } else {
            return AnyView(loading)
        }
    }

    func details(for viewModel: CurrentWeatherRowViewModel) -> some View {
        CurrentWeatherRow(viewModel: viewModel)
    }

    var loading: some View {
        Text("Loading \(viewModel.city)'s weather...")
            .foregroundColor(.gray)
    }
}
