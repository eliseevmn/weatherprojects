import UIKit

protocol DetailCityPresentationLogic {
    func presentCity(response: DetailCity.ShowDetails.Response)
}

class DetailCityPresenter: DetailCityPresentationLogic {
    
    weak var viewController: DetailCityDisplayLogic?
    var worker: DetailCityWorker?
    
    // MARK: Present city
    func presentCity(response: DetailCity.ShowDetails.Response) {
        worker = DetailCityWorker()
        
        
        guard let myCity = response.myCity else { return }
        guard let dailyList = myCity.dailyWeather?.list else { return }
        guard let displayedWeather = worker?.getDisplayedWeather(from: dailyList) else {
            print("Не пошло")
            return
        }
        
        let viewModel = DetailCity.ShowDetails.ViewModel(myCity: myCity, displayedMyCityWeather: displayedWeather)
        viewController?.displayDetailsWeather(viewModel: viewModel)
    }
}
