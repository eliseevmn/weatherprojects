import Foundation

class DetailCellViewModel: DetailCellViewProtocol {

    private let dailyWeather: DailyList

    var date: String? {
        let timeInterval = Double(dailyWeather.date)
        return dateFormatter.string(from: Date(timeIntervalSince1970: timeInterval))
    }

    var weatherIcon: String? {
        let weatherIcon = dailyWeather.weather[0].icon
        return weatherIcon
    }

    var temperature: String? {
        let tempMaxFromJson = dailyWeather.main.tempMax
        let tempMinFromJson = dailyWeather.main.tempMin
        let tempMaxCelcium = String(format: "%.0f℃", tempMaxFromJson - 273.15)
        let tempMinCelcium = String(format: "%.0f℃", tempMinFromJson - 273.15)
        return "Max: \(tempMaxCelcium), min: \(tempMinCelcium)"
    }

    var description: String? {
        return dailyWeather.weather[0].main
    }

    private var dateFormatter: DateFormatter = {
        let date = DateFormatter()
        date.dateFormat = "HH:mm EEEE"
        return date
    }()

    required init(dailyWeather: DailyList) {
        self.dailyWeather = dailyWeather
    }
}
