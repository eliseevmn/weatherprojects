import UIKit

protocol AddCityDisplayLogic: class {
    func displayCIties(viewModel: AddCity.Something.ViewModel)
}

class AddCityViewController: UIViewController {
    
    // MARK: - Outlets
    private var tableView: UITableView = UITableView(frame: .zero, style: .plain)
    
    // MARK: - Properties
    private let searchController = UISearchController(searchResultsController: nil)
    private var timer: Timer?
    private var displayedCities: [DisplayedSearchCities] = []
    
    var interactor: AddCityBusinessLogic?
    var router: (NSObjectProtocol & AddCityRoutingLogic & AddCityDataPassing)?
    
    private var cities: [DisplayedSearchCities] = []
    
    // MARK: - Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupSearchController()
    }
    
    func fetchCities(searchCity: String) {
        interactor?.fetchCities(searchCity: searchCity)
    }
    
    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = AddCityInteractor()
        let presenter = AddCityPresenter()
        let router = AddCityRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
}

// MARK: - Setup UI
extension AddCityViewController {
    private func setupUI() {
        view.addSubview(tableView)
        tableView.anchor(top: view.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        tableView.register(AddCityTableViewCell.self, forCellReuseIdentifier: AddCityTableViewCell.reuseId)
        
        navigationItem.title = "Поиск"
        navigationController?.navigationBar.isHidden = false
    }
    
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.placeholder = "Search City"
        searchController.searchBar.delegate = self
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension AddCityViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddCityTableViewCell.reuseId, for: indexPath) as! AddCityTableViewCell
        
        let city = cities[indexPath.row]
        cell.configure(city: city)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.routeToMyCityList(indexPath: indexPath)
    }
}

// MARK: - UISearchBarDelegate, UISearchResultsUpdating
extension AddCityViewController: UISearchBarDelegate, UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text, query.count > 2 else {
            return
        }
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false, block: { [weak self] (_) in
            self?.fetchCities(searchCity: query)
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        })
    }
}

extension AddCityViewController: AddCityDisplayLogic {
    func displayCIties(viewModel: AddCity.Something.ViewModel) {
        self.cities = viewModel.displayedSearchCities
    }
}
