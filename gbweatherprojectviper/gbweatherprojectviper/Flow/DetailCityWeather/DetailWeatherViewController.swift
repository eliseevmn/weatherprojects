import UIKit

protocol DetailWeatherViewProtocol {
    func displayCurrentWeather(with cityWeather: MyCity?)
    func reloadData()
}

class DetailWeatherViewController: UIViewController {
    
    var presentor: DetailWeatherPresenterProtocol!
    
    private var tableView: UITableView = UITableView(frame: .zero, style: .plain)
    private var returnButton: CustomButton = {
        let button = CustomButton(widthAndHeight: 50, image: #imageLiteral(resourceName: "return"))
        button.layer.opacity = 0.7
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return button
    }()
    var currenWeatherView: CurrentView = {
        let view = CurrentView()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
        presentor.showWeatherDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    private func setupUI() {
        view.addSubview(tableView)
        view.addSubview(currenWeatherView)
        currenWeatherView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                                  leading: view.leadingAnchor,
                                  bottom: nil,
                                  trailing: view.trailingAnchor)
        tableView.anchor(top: currenWeatherView.bottomAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.showsVerticalScrollIndicator = false
        
        tableView.register(DetailTableViewCell.self, forCellReuseIdentifier: DetailTableViewCell.reuseId)
        
        view.addSubview(returnButton)
        returnButton.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                            leading: view.leadingAnchor,
                            bottom: nil,
                            trailing: nil,
                            padding: .init(top: 20, left: 16, bottom: 0, right: 0))
    }
    
    @objc private func handleDismiss() {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITableViewDataSource
extension DetailWeatherViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentor.weatherList
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailTableViewCell.reuseId,
                                                 for: indexPath) as! DetailTableViewCell
        presentor.selectRow(at: indexPath)
        cell.configure(with: presentor.dailyWeather)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension DetailWeatherViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension DetailWeatherViewController: DetailWeatherViewProtocol {
    func displayCurrentWeather(with cityWeather: MyCity?) {
        currenWeatherView.configureData(cityWeather: cityWeather)
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
