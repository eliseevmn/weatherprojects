import UIKit

class MyCityListTableViewCell: UITableViewCell {
    
    static let reuseId = "MyCityListTableViewCell"
    
    private let cityNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        return label
    }()
    
    private let currentTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return label
    }()
    
    private let currentTempLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        return label
    }()
    
    private var iconWeather: WebImageView = {
        let imageView = WebImageView()
        return imageView
    }()
    
    private var dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "HH:mm"
        return dt
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        configureUI()
    }
    
    private func configureUI() {
        iconWeather.heightAnchor.constraint(equalToConstant: 60).isActive = true
        iconWeather.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        let verticalStackView = UIStackView(arrangedSubviews: [
            currentTimeLabel,
            cityNameLabel
        ])
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .leading
        verticalStackView.spacing = 10
        let horizontalStackView = UIStackView(arrangedSubviews: [
            iconWeather,
            verticalStackView,
            currentTempLabel
        ])
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 10
        horizontalStackView.alignment = .center
        addSubview(horizontalStackView)
        horizontalStackView.anchor(top: topAnchor,
                                   leading: leadingAnchor,
                                   bottom: bottomAnchor,
                                   trailing: trailingAnchor,
                                   padding: .init(top: 10, left: 16, bottom: 10, right: 16))
    }
    
    func configure(with city: MyCity?) {
        cityNameLabel.text = city?.name
        
        let tempFromJson = city?.currentWeather?.main.temp ?? 0.0
        let tempCelcium = String(format:"%.0f℃", tempFromJson - 273.15)
        currentTempLabel.text = tempCelcium
        
        let timeInterval = Double(city?.currentWeather?.dt ?? 0)
        currentTimeLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: timeInterval))
        
        iconWeather.set(iconString: city?.currentWeather?.weather[0].icon)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
