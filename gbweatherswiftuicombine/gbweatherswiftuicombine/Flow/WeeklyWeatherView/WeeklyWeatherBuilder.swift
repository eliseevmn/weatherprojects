import SwiftUI

enum WeeklyWeatherBulder {
    static func makeCurrentWeatherView(
        withCity city: String,
        weatherFetcher: WeatherFetcherProtocol) -> some View {
        let viewModel = CurrentWeatherViewModel(
          city: city,
          weatherFetcher: weatherFetcher)
        return CurrentWeatherView(viewModel: viewModel)
    }
}
