import UIKit

protocol DetailViewModelProtocol {
    var cityWeather: MyCity? { get }
    init(city: MyCity)
    func numberOfRows() -> Int
    func cellViewModel(for indexPath: IndexPath) -> DetailCellViewProtocol?
}
