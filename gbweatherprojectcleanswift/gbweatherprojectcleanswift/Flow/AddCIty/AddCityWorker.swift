import UIKit

class AddCityWorker {
    
    func getDisplayedCities(from cities: [CityResponse]) -> [DisplayedSearchCities] {
        var displayedCities: [DisplayedSearchCities] = []
        
        cities.forEach { (city) in
            let cityName = city.name
            
            let displayedCity = DisplayedSearchCities(name: cityName)
            displayedCities.append(displayedCity)
        }
        return displayedCities
    }
}
