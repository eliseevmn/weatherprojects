//
//  ButtonModifier.swift
//  Honeymoon
//
//  Created by MAC on 28.07.2020.
//  Copyright © 2020 MAC. All rights reserved.
//

import SwiftUI

struct ButtonModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.headline)
            .padding()
            .frame(minWidth: 0, maxWidth: .infinity)
            .background(
                Capsule().fill(Color.pink)
        )
            .foregroundColor(.white)
    }
}
