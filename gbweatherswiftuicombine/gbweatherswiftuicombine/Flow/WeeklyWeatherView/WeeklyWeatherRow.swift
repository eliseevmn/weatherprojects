import SwiftUI

struct WeeklyWeatherRow: View, Identifiable {

    // MARK: - Properties
    let id = UUID()
    let viewModel: WeeklyWeatherRowViewModel

    // MARK: - Init
    init(viewModel: WeeklyWeatherRowViewModel) {
        self.viewModel = viewModel
    }

    // MARK: - Body
    var body: some View {
        VStack {
            HStack {
                Text("\(viewModel.day)")
                Text("\(viewModel.month)")
            } // HStack
            .font(.largeTitle)

            VStack {
                Text("\(viewModel.title)")
                    .font(.footnote)
                Text("\(viewModel.fullDescription)")
                    .font(.headline)
            } // VStack
            .padding(.leading, 8)

            Spacer()

            Text("\(viewModel.temperature)°")
                .font(.largeTitle)
        } // VStack
        .frame(minWidth: 0,
               maxWidth: .infinity,
               minHeight: 0,
               maxHeight: .infinity)
        .padding(.top, 30)
        .padding(.bottom, 30)
        .foregroundColor(.black)
        .background(Color(.secondarySystemBackground))
        .cornerRadius(25)
        .overlay(
            RoundedRectangle(cornerRadius: 25)
                .stroke(Color.blue, lineWidth: 2)
        )
        .padding(20)
    } // Body
}
