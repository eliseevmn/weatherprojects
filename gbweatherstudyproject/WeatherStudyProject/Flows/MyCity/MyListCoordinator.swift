import UIKit

class MyListCoordinator: BaseCoordinator {

    var rootController: UINavigationController?
    var onFinishFlow: (() -> Void)?

    override func start() {
        showMyListModule()
    }

    private func showMyListModule() {
        let controller = MyCityListViewController()

        controller.onAddCity = { [weak self] in
            self?.showAddCityModule()
        }

        controller.onDetailCity = { [weak self] viewModel in
            self?.showDetailCityModule(viewModel: viewModel)
        }

        let rootController = UINavigationController(rootViewController: controller)
        setAsRoot(rootController)
        self.rootController = rootController
    }

    private func showAddCityModule() {
        let controller = AddCityViewController()
        rootController?.pushViewController(controller, animated: true)
    }

    private func showDetailCityModule(viewModel: DetailViewModelProtocol?) {
        let controller = DetailViewController()
        controller.viewModel = viewModel
        rootController?.pushViewController(controller, animated: true)
    }
}
