import UIKit

protocol MyCityListRoutingLogic {
    func routeToCityDetails(indexPath: IndexPath)
    func routeToAddCityController()
}

protocol MyCityListDataPassing {
    var dataStore: MyCityListDataStore? { get }
}

class MyCityListRouter: NSObject, MyCityListRoutingLogic, MyCityListDataPassing {
    
    weak var viewController: MyCityListViewController?
    var dataStore: MyCityListDataStore?
    
    // MARK: Routing
    
    func routeToCityDetails(indexPath: IndexPath) {
        let destinationVC = DetailCityViewController()
        var destinationDS = destinationVC.router?.dataStore!
        passDataToSomewhere(source: dataStore!, destination: &(destinationDS)!, indexPath: indexPath)
        navigateToDetail(source: viewController!, destination: destinationVC)
    }
    
    func routeToAddCityController() {
        let destinationVC = AddCityViewController()
        navigateToAddController(source: viewController!, destination: destinationVC)
    }
    
    // MARK: Navigation
    private func navigateToDetail(source: MyCityListViewController, destination: DetailCityViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }
    
    private func navigateToAddController(source: MyCityListViewController, destination: AddCityViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }
    
    // MARK: Passing data
    private func passDataToSomewhere(source: MyCityListDataStore, destination: inout DetailCityDataStore, indexPath: IndexPath) {
        destination.myCity = source.cities[indexPath.row]
    }
}
