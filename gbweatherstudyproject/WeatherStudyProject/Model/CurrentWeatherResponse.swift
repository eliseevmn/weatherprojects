import Foundation

// MARK: - CurrentWeatherResponse
struct CurrentWeatherResponse: Decodable {
//    let coord: Coord
    let weather: [Weather]
//    let base: String
    let main: Main
//    let visibility: Int
//    let wind: Wind
//    let rain: Rain
//    let clouds: Clouds
    var date: Int
//    let sys: Sys
//    let timezone, id: Int
    let name: String
//    let cod: Int

    enum CodingKeys: String, CodingKey {
        case weather, main, name
        case date = "dt"
    }
}

// MARK: - Clouds
struct Clouds: Decodable {
    let all: Int
}

// MARK: - Coord
struct Coord: Decodable {
    let lon, lat: Double
}

// MARK: - Main
struct Main: Decodable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, humidity: Int

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure, humidity
    }
}

// MARK: - Rain
struct Rain: Decodable {
    let the3H: Double

    enum CodingKeys: String, CodingKey {
        case the3H = "3h"
    }
}

// MARK: - Sys
struct Sys: Decodable {
    let type, idSys: Int
    let country: String
    let sunrise, sunset: Int

    enum CodingKeys: String, CodingKey {
        case idSys = "id"
        case type, country, sunrise, sunset
    }
}

// MARK: - Weather
struct Weather: Decodable {
    let idWeather: Int
    let main, weatherDescription, icon: String

    enum CodingKeys: String, CodingKey {
        case idWeather = "id"
        case main
        case weatherDescription = "description"
        case icon
    }
}

// MARK: - Wind
struct Wind: Decodable {
    let speed: Double
    let deg: Int
}
