import Foundation

enum NetworkRouter {
    
    case currentWeather(cityName: String)
    case fiveDailyWeather(cityName: String)
    
    var scheme: String {
        switch self {
        case .currentWeather, .fiveDailyWeather:
            return "https"
        }
    }
    
    
    var host: String {
        switch self {
        case .currentWeather, .fiveDailyWeather:
            return "api.openweathermap.org"
        }
    }
    
    var path: String {
        switch self {
        case .currentWeather:
            return "/data/2.5/weather"
        case .fiveDailyWeather:
            return "/data/2.5/forecast"
        }
    }

    var parameters: [URLQueryItem] {
        let appId = "504c451dc108c337e8742b9a38cea71c"
        switch self {
        case .currentWeather(let cityName), .fiveDailyWeather(let cityName):
            return [
                URLQueryItem(name: "q", value: cityName),
                URLQueryItem(name: "appid", value: appId)
            ]
        }
    }
    
    var method: String {
        switch self {
        case .currentWeather, .fiveDailyWeather:
            return "GET"
        }
    }
}
