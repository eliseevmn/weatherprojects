import Foundation

class AddCityViewModel: AddCityViewModelProtocol {

    // MARK: - Properties
    var cities: Box<[CityResponse]>
    private var allCities: [CityResponse] = []
    private var indexPath: IndexPath?

    // MARK: - Network functions
    func fetchCity(searchCity: String, completion: @escaping () -> Void) {
        allCities = LoadServiceJSONFromFile.load("cities.json")

        DispatchQueue.main.async {
            self.cities.value = self.allCities.filter({ (city) -> Bool in
                return city.name.lowercased().contains(searchCity.lowercased())
            })
            completion()
        }
    }

    // MARK: - Cell functions
    func cellViewModel(for indexPath: IndexPath) -> AddCityCellViewModelProtocol? {
        let city = cities.value[indexPath.row]
        return AddCityCellViewModel(city: city)
    }

    func selectRow(for indexPath: IndexPath) {
        self.indexPath = indexPath
    }

    func numberOfRows() -> Int {
        return cities.value.count
    }

    required init() {
        cities = Box([])
    }

    func viewModelForSelectedRow() -> CityResponse? {
        guard let indexPath = indexPath else { return CityResponse(name: "") }
        let city = cities.value[indexPath.row]
        return city
    }
}
