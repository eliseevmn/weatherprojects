import Foundation

class NetworkService {
    
    class func sendRequestForWeather<T: Decodable>(router: NetworkRouter, completion: @escaping (T) -> Void) {

        var urlComponents = URLComponents()
        urlComponents.scheme = router.scheme
        urlComponents.host = router.host
        urlComponents.path = router.path
        urlComponents.queryItems = router.parameters
        
        guard let url = urlComponents.url else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = router.method
        
        let configuration = URLSessionConfiguration.default
        configuration.allowsCellularAccess = true
        let session = URLSession(configuration: configuration)
        
        session.dataTask(with: urlRequest) { data, response, error in
            if let error = error {
                print("\(error)")
                return
            }
            guard let data = data else { return }
            let decoder = JSONDecoder()
            do {
                let objectWeather = try decoder.decode(T.self, from: data)
                completion(objectWeather)
            } catch let catchError {
                print(catchError.localizedDescription)
            }
        }.resume()
    }
}
