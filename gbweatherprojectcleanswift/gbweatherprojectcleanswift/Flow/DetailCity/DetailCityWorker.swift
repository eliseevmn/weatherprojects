import UIKit

class DetailCityWorker {
    
    private var dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "HH:mm"
        return dt
    }()
    
    func getDisplayedWeather(from cityDetailWeather: [DailyList]) -> [DisplayedDetailWeather] {
        
        var displayedWeather: [DisplayedDetailWeather] = []
        
        cityDetailWeather.forEach { (weather) in
            let date = fetchDailyTime(from: weather)
            let description = weather.weather[0].main
            let temperature = fetchTempToCelsium(from: weather)
            let iconWeather = weather.weather[0].icon
            
            let displayWeather = DisplayedDetailWeather(date: date, temperature: temperature, description: description, icon: iconWeather)
            displayedWeather.append(displayWeather)
        }
        return displayedWeather
    }
    
    private func fetchTempToCelsium(from weather: DailyList?) -> String {
        let tempMaxFromJson = weather?.main.tempMax ?? 0.0
        let tempMinFromJson = weather?.main.tempMin ?? 0.0
        let tempMaxCelcium = String(format:"%.0f℃", tempMaxFromJson - 273.15)
        let tempMinCelcium = String(format:"%.0f℃", tempMinFromJson - 273.15)
        return "Max: \(tempMaxCelcium), min: \(tempMinCelcium)"
    }
    
    private func fetchDailyTime(from weather: DailyList?) -> String {
        let timeInterval = Double(weather?.dt ?? 0)
        return dateFormatter.string(from: Date(timeIntervalSince1970: timeInterval))
    }
}
