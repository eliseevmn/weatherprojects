import UIKit

typealias DisplayedCity = MyCityList.FetchCities.ViewModel.DisplayedCity

enum MyCityList {
    
    enum FetchCities {
        struct Request {
        }
        
        struct Response {
            let cities: [MyCity]
        }
        
        struct ViewModel {
            
            struct DisplayedCity {
                let cityName: String?
                let currentTime: String?
                let currentTemp: String?
                let iconWeather: String?
            }
            let displayedCities: [DisplayedCity]
        }
    }
}
