import UIKit

protocol MyCityListPresenterProtocol: class {
    var citiesCount: Int { get }
    var myCity: MyCity? { get }
    init(view: MyCityListViewProtocol)
    func showCitiesList(cityName: String)
    func selectRow(at indexPath: IndexPath)
    func showDetailController()
    func showAddCityController()
    func didAddCity(city: CityResponse)
    func refrestWeatherData()
}

class MyCityListPresenter: MyCityListPresenterProtocol {
    
    var view: MyCityListViewProtocol
    var interactor: MyCityListInteractorProtocol!
    var router: MyCityListRouterProtocol?
    var presenterAdd: AddCityPresenterProtocol?

    var citiesCount: Int {
        return citiesList.count
    }
    
    var myCity: MyCity? {
        guard let indexPath = indexPath else { return nil }
        return getCity(at: indexPath)
    }
    var cityResponse: CityResponse?
    
    private var citiesList: [MyCity] = []
    private var indexPath: IndexPath?
    
    required init(view: MyCityListViewProtocol) {
        self.view = view
    }
    
    func showCitiesList(cityName: String) {
        interactor.fetchWeatherCities(cityName: cityName)
    }
    
    func selectRow(at indexPath: IndexPath) {
        self.indexPath = indexPath
    }
    
    private func getCity(at indexPath: IndexPath) -> MyCity? {
        if citiesList.indices.contains(indexPath.row) {
            return citiesList[indexPath.row]
        }
        return nil
    }
    
    func showDetailController() {
        guard let city = myCity else { return }
        router?.pushToDetailController(on: view, city: city)
    }
    
    func showAddCityController() {
        router?.pushToAddCityController(on: view)
    }
    
    func didAddCity(city: CityResponse) {
        cityResponse = city
        interactor.fetchWeatherCities(cityName: cityResponse?.name ?? "")
    }
    
    func refrestWeatherData() {
        citiesList.forEach { (city) in
            interactor.fetchWeatherCities(cityName: city.name)
        }
        view.reloadData()
    }
}

// MARK: - Outputs to view
extension MyCityListPresenter: MyCityListInteractorOutputProtocol {
    func citiesWeatherDidReceive(_ cities: [MyCity]) {
        self.citiesList = cities
        view.reloadData()
    }
}
