import UIKit

typealias DisplayedSearchCities = AddCity.Something.ViewModel.DisplayedSearchCities

enum AddCity {
    
    enum Something {
        struct Request {
        }
        
        struct Response {
            let cities: [CityResponse]
        }
        
        struct ViewModel {
            
            struct DisplayedSearchCities {
                let name: String?
            }
            
            let displayedSearchCities: [DisplayedSearchCities]
        }
    }
}
