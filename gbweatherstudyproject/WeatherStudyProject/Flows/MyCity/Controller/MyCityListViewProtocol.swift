import Foundation

protocol MyCityListViewProtocol {
    var cities: [MyCity] { get }
    func fetchCity(city: String, completion: @escaping() -> Void)
    func numberOfRows() -> Int
    func cellViewModel(for indexPath: IndexPath) -> MyCityListCellViewProtocol?
    func selectRow(for indexPath: IndexPath)
    func viewModelForSelectedRow() -> DetailViewModelProtocol?
}
