import UIKit

@objc protocol AddCityRoutingLogic {
    func routeToMyCityList(indexPath: IndexPath)
}

protocol AddCityDataPassing {
    var dataStore: AddCityDataStore? { get }
}

class AddCityRouter: NSObject, AddCityRoutingLogic, AddCityDataPassing {
    
    weak var viewController: AddCityViewController?
    var dataStore: AddCityDataStore?
    
    // MARK: Routing
    func routeToMyCityList(indexPath: IndexPath) {
        let index = viewController!.navigationController!.viewControllers.count - 2
        let destinationVC = viewController?.navigationController?.viewControllers[index] as! MyCityListViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToMyCityList(source: dataStore!, destination: &destinationDS, indexPath: indexPath)
        navigateToMyCityList(source: viewController!, destination: destinationVC)
    }
    
    // MARK: Navigation
    func navigateToMyCityList(source: AddCityViewController, destination: MyCityListViewController) {
        source.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Passing data
    private func passDataToMyCityList(source: AddCityDataStore, destination: inout MyCityListDataStore, indexPath: IndexPath) {
        destination.searchCity = source.allCities[indexPath.row]
    }
}
