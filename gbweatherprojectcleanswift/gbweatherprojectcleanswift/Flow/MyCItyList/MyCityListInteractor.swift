import UIKit

protocol MyCityListBusinessLogic {
    func fetchCities(cityName: String)
}

protocol MyCityListDataStore {
    var cities: [MyCity] { get set }
    var searchCity: CityResponse? { get set }
}

class MyCityListInteractor: MyCityListBusinessLogic, MyCityListDataStore {
    
    var presenter: MyCityListPresentationLogic?
    var worker: MyCityListWorker?
    
    var cities: [MyCity] = []
    var searchCity: CityResponse?
    
    // MARK: Fetch Cities
    func fetchCities(cityName: String) {
        worker = MyCityListWorker()
        
        NetworkService.sendRequestForWeather(router: .currentWeather(cityName: cityName)) { [weak self] (currentWeather: CurrentWeatherResponse) in
            
            NetworkService.sendRequestForWeather(router: .fiveDailyWeather(cityName: cityName)) { (dailyWeather: DailyWeatherResponse) in
                
                let myCity = MyCity(name: cityName, dailyWeather: dailyWeather, currentWeather: currentWeather)
                
                if self?.cities.contains(where: { (city) -> Bool in
                    return city.name.contains(myCity.name)
                }) ?? false {
                    if let row = self?.cities.firstIndex(where: {$0.name == myCity.name}) {
                        self?.cities[row].currentWeather = myCity.currentWeather
                    }
                    let response = MyCityList.FetchCities.Response(cities: self?.cities ?? [])
                    self?.presenter?.presentCities(response: response)
                } else {
                    self?.cities.append(myCity)
                    let response = MyCityList.FetchCities.Response(cities: self?.cities ?? [])
                    self?.presenter?.presentCities(response: response)
                }
            }
        }
    }
}
