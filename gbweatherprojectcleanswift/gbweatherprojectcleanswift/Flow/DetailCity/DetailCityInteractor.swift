import UIKit

protocol DetailCityBusinessLogic {
    func showDetails()
}

protocol DetailCityDataStore {
    var myCity: MyCity? { get set }
}

class DetailCityInteractor: DetailCityBusinessLogic, DetailCityDataStore {
    
    var presenter: DetailCityPresentationLogic?
    var worker: DetailCityWorker?
    
    var myCity: MyCity?
    
    // MARK: Do something
    func showDetails() {
        worker = DetailCityWorker()
        let response = DetailCity.ShowDetails.Response(myCity: myCity)
        presenter?.presentCity(response: response)
    }
}
