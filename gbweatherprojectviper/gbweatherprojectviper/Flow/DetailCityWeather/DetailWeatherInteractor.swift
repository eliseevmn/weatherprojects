import Foundation

protocol DetailWeatherInteractorProtocol: class {
    var myCity: MyCity { get set }
    init(presentor: DetailWeatherInteractorOutputProtocol, city: MyCity)
    func provideWeatherDetails()
}

protocol DetailWeatherInteractorOutputProtocol: class {
    func receiveWeatherDetails(weatherDetails: WeatherDetailsData)
}

class DetailWeatherInteractor: DetailWeatherInteractorProtocol {
    
    unowned var presenter: DetailWeatherInteractorOutputProtocol
    var myCity: MyCity
    
    required init(presentor: DetailWeatherInteractorOutputProtocol, city: MyCity) {
        self.presenter = presentor
        self.myCity = city
    }
    
    func provideWeatherDetails() {
        let weatherDetails = WeatherDetailsData(city: myCity)
        presenter.receiveWeatherDetails(weatherDetails: weatherDetails)
    }
}
