import UIKit

class DetailViewModel: DetailViewModelProtocol {

    // MARK: - Properties
    var cityWeather: MyCity? {
        return city
    }
    private var city: MyCity

    // MARK: - Init
    required init(city: MyCity) {
        self.city = city
    }

    // MARK: - Cell functions
    func numberOfRows() -> Int {
        return city.dailyWeather?.list.count ?? 0
    }

    func cellViewModel(for indexPath: IndexPath) -> DetailCellViewProtocol? {
        guard let weather = city.dailyWeather?.list[indexPath.row] else { return nil }
        return DetailCellViewModel(dailyWeather: weather)
    }
}
