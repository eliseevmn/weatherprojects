import Foundation
import SwiftUI

struct WeeklyWeatherRowViewModel: Identifiable {

    // MARK: - Properties
    private let item: WeeklyForecastResponse.Item

    var id: String {
        return day + temperature + title
    }

    var day: String {
        return dayFormatter.string(from: item.date)
    }

    var month: String {
        return monthFormatter.string(from: item.date)
    }

    var temperature: String {
        return String(format: "%.1f", item.main.temp)
    }

    var title: String {
        guard let title = item.weather.first?.main.rawValue else {
            return ""
        }
        return title
    }

    var fullDescription: String {
        guard let description = item.weather.first?.weatherDescription else {
            return ""
        }
        return description
    }

    // MARK: - Init
    init(item: WeeklyForecastResponse.Item) {
        self.item = item
    }
}

extension WeeklyWeatherRowViewModel: Hashable {
    static func == (lhs: WeeklyWeatherRowViewModel, rhs: WeeklyWeatherRowViewModel) -> Bool {
        return lhs.day == rhs.day
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(self.day)
    }
}
