import UIKit

protocol AddCityRouterProtocol: class {
    static func createModule(with delegate: MyCityListPresenterProtocol) -> UIViewController
    func returnToMyCityListController(from view: AddCityViewProtocol)
}

class AddCityRouter: AddCityRouterProtocol {
    
    // MARK: Static methods
    static func createModule(with delegate: MyCityListPresenterProtocol) -> UIViewController {
        
        let viewController = AddCityViewController()
        let configurator: AddCityConfiguratorProtocol = AddCityConfigurator()
        configurator.configure(with: viewController, delegate: delegate)
        
        return viewController
    }
    
    func returnToMyCityListController(from view: AddCityViewProtocol) {
        guard let view = view as? UIViewController else {
            fatalError()
        }
        view.navigationController?.popViewController(animated: true)
    }
    
    func dismissAddCityContoller(from view: UINavigationController, completion: (() -> Void)?) {
        let navController = UINavigationController()
        navController.popViewController(animated: true)
    }
}
