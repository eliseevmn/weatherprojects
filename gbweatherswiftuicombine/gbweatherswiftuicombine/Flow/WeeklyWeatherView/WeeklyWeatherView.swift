import SwiftUI
import Combine

struct WeeklyWeatherView: View {

    // MARK: - Properties
    @ObservedObject var viewModel: WeeklyWeatherViewModel

    @GestureState private var dragState = DragState.inactive
    private let dragAreaThreshold: CGFloat = 65.0
    @State private var lastCardIndex: Int = 1
    @State private var cardRemovalTransition = AnyTransition.trailingBottom
    @State var cardViews: [WeeklyWeatherRow] = []

    // MARK: - Init
    init(viewModel: WeeklyWeatherViewModel) {
        self.viewModel = viewModel
    }

    // MARK: - Body
    var body: some View {
        NavigationView {
            VStack {
                SearchView(searchTerm: self.$viewModel.city)

                if viewModel.dataSource.isEmpty {
                    emptySection
                        .onAppear {
                            self.removeCardViews()
                        }
                } else {
                    cityWeatherSection
                    forecastSection
                }
                Spacer()
            } // VStack
            .navigationBarTitle("Weather")
        } // Navigation View
    } // Body

    // MARK: - Card Views
    private func moveCards() {
        self.cardViews.removeFirst()
        self.lastCardIndex += 1
        let data = viewModel.dataSource[lastCardIndex % viewModel.dataSource.count]
        let newCard = WeeklyWeatherRow(viewModel: data)
        cardViews.append(newCard)
    }

    private func isTopCard(cardView: WeeklyWeatherRow) -> Bool {
        guard let index = cardViews.firstIndex(where: { $0.id == cardView.id }) else {
            return false
        }

        return index == 0
    }

    private func getViews() -> [WeeklyWeatherRow] {
        for index in 0..<2 {
            self.cardViews.append(WeeklyWeatherRow(viewModel: viewModel.dataSource[index]))
        }
        return self.cardViews
    }

    private func removeCardViews() {
        self.cardViews.removeAll()
        self.lastCardIndex = 0
    }
}

// MARK: - Custom View
private extension WeeklyWeatherView {

    var searchField: some View {
        HStack {
            TextField("Search...", text: $viewModel.city)
        }
    } 

    var cityWeatherSection: some View {
        NavigationLink(destination: viewModel.currentWeatherView) {
            VStack {
                HStack {
                    VStack(alignment: .leading) {
                        Text(viewModel.city)
                        Text("Погода сегодня")
                            .font(.caption)
                            .foregroundColor(.gray)
                    }

                    Spacer()

                    Image(systemName: "chevron.right")
                        .padding(.trailing, 10)
                }

                Divider()
            }
            .frame(width: UIScreen.main.bounds.width - 40)
        }
    }

    var emptySection: some View {
        Section {
            Text("Результаты отсутствуют")
                .multilineTextAlignment(.leading)
                .foregroundColor(.gray)
        }
    }

    var forecastSection: some View {
        ZStack {
            ForEach(cardViews) { cardView in
                cardView
                    .zIndex(self.isTopCard(cardView: cardView) ? 1 : 0)
                    .offset(x: self.isTopCard(cardView: cardView) ? self.dragState.translation.width : 0, y: self.isTopCard(cardView: cardView) ?  self.dragState.translation.height : 0)
                    .scaleEffect(self.dragState.isDragging && self.isTopCard(cardView: cardView) ? 0.85 : 1.0)
                    .rotationEffect(Angle(degrees: self.isTopCard(cardView: cardView) ? Double(self.dragState.translation.width / 12) : 0))
                    .animation(.interpolatingSpring(stiffness: 120, damping: 120))
                    .gesture(LongPressGesture(minimumDuration: 0.01)
                        .sequenced(before: DragGesture())
                        .updating(self.$dragState, body: { (value, state, transaction) in

                            switch value {
                            case .first(true):
                                state = .pressing
                            case .second(true, let drag):
                                state = .dragging(translation: drag?.translation ?? .zero)
                            default:
                                break
                            }
                        })
                        .onChanged({ (value) in
                            guard case .second(true, let drag?) = value else {
                                return
                            }

                            if drag.translation.width < -self.dragAreaThreshold {
                                self.cardRemovalTransition = .laedingBottom
                            }

                            if drag.translation.width > self.dragAreaThreshold {
                                self.cardRemovalTransition = .trailingBottom
                            }
                        })
                        .onEnded({ (value) in
                            guard case .second(true, let drag?) = value else {
                                return
                            }
                            if drag.translation.width < -self.dragAreaThreshold || drag.translation.width > self.dragAreaThreshold {
                                self.moveCards()
                            }
                        })
                    ).transition(self.cardRemovalTransition)

            }
        }.onAppear {
            self.cardViews = self.getViews()
        }
    }
}
