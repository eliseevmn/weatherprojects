import UIKit

typealias DisplayedDetailWeather = DetailCity.ShowDetails.ViewModel.DisplayedDetailWeather

enum DetailCity {
    // MARK: Use cases
    
    enum ShowDetails {
        struct Request {

        }
        
        struct Response {
            let myCity: MyCity?
        }
        
        struct ViewModel {
            let myCity: MyCity?
                
            struct DisplayedDetailWeather {
                let date: String?
                let temperature: String?
                let description: String?
                let icon: String?
            }
            let displayedMyCityWeather: [DisplayedDetailWeather]
        }
    }
}
