import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {

    // MARK: - Properties
    private let coordinate: CLLocationCoordinate2D

    // MARK: - Init
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }

    // MARK: - Functions
    func makeUIView(context: Context) -> MKMapView {
        MKMapView()
    }

    func updateUIView(_ view: MKMapView, context: Context) {
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: coordinate, span: span)

        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        view.addAnnotation(annotation)

        view.setRegion(region, animated: true)
    }
}
