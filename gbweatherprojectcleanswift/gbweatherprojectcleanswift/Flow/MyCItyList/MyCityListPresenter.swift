import UIKit

protocol MyCityListPresentationLogic {
    func presentCities(response: MyCityList.FetchCities.Response)
}

class MyCityListPresenter: MyCityListPresentationLogic {
    
    weak var viewController: MyCityListDisplayLogic?
    var worker: MyCityListWorker?
    
    // MARK: Do something
    
    func presentCities(response: MyCityList.FetchCities.Response) {
        worker = MyCityListWorker()
        
        guard let displayedCities = worker?.getDisplayedCities(from: response.cities) else { return }
        
        let viewModel = MyCityList.FetchCities.ViewModel(displayedCities: displayedCities)
        viewController?.displayCitiesList(viewModel: viewModel)
    }
}
