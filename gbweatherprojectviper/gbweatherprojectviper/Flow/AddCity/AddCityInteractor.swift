import Foundation

protocol AddCityInteractorProtocol: class {
    init(presentor: AddCityInteractorOutputProtocol)
    func fetchWeatherCities(searchCity: String)
}

protocol AddCityInteractorOutputProtocol: class {
    func citiesDidReceive(searchCity: [CityResponse])
}

class AddCityInteractor: AddCityInteractorProtocol {
    
    unowned var presenter: AddCityInteractorOutputProtocol
    private var allCities: [CityResponse] = []
    
    required init(presentor: AddCityInteractorOutputProtocol) {
        self.presenter = presentor
    }
    
    func fetchWeatherCities(searchCity: String) {
        allCities = LoadServiceJSONFromFile.load("cities.json")
        
        self.allCities = self.allCities.filter({ (city) -> Bool in
            return city.name.lowercased().contains(searchCity.lowercased())
        })
        presenter.citiesDidReceive(searchCity: allCities)
    }
}
