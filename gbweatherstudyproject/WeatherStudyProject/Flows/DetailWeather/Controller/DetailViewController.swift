import UIKit

class DetailViewController: UIViewController {

    // MARK: - Outlets
    private let cityNameLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    private var tableView: UITableView = UITableView(frame: .zero, style: .plain)
    private var returnButton: CustomButton = {
        let button = CustomButton(widthAndHeight: 50, image: #imageLiteral(resourceName: "return"))
        button.layer.opacity = 0.7
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return button
    }()
    lazy var currentWeatherView: CurrentView = {
        let view = CurrentView(cityWeather: viewModel.cityWeather)
        return view
    }()

    // MARK: - Properties
    var viewModel: DetailViewModelProtocol!

    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = viewModel.cityWeather?.name
    }

    // MARK: - Actions
    @objc private func handleDismiss() {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension DetailViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: DetailTableViewCell.reuseId,
            for: indexPath) as? DetailTableViewCell else {
                return UITableViewCell()
        }
        let cellViewModel = viewModel.cellViewModel(for: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Setup UI
extension DetailViewController {
    private func setupUI() {
        view.addSubview(tableView)
        view.addSubview(currentWeatherView)
        currentWeatherView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                                  leading: view.leadingAnchor,
                                  bottom: nil,
                                  trailing: view.trailingAnchor)
        tableView.anchor(top: currentWeatherView.bottomAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.showsVerticalScrollIndicator = false

        tableView.register(DetailTableViewCell.self, forCellReuseIdentifier: DetailTableViewCell.reuseId)

        view.addSubview(returnButton)
        returnButton.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 20, left: 16, bottom: 0, right: 0))
    }
}
