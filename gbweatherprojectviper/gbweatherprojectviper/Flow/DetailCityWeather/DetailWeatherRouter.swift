import UIKit

// MARK: Router Input (Presenter -> Router)
protocol DetailWeatherRouterProtocol: class {
    
    static func createModule(with city: MyCity) -> UIViewController
}

class DetailWeatherRouter: DetailWeatherRouterProtocol {
    
    // MARK: Static methods
    static func createModule(with city: MyCity) -> UIViewController {
        
        let viewController = DetailWeatherViewController()
        let configurator: DetailWeatherConfiguratorProtocol = DetailWeatherConfigurator()
        configurator.configure(with: viewController, and: city)
        
        return viewController
    }
    
}
