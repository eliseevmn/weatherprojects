import UIKit

protocol DetailCityDisplayLogic: class {
    func displayDetailsWeather(viewModel: DetailCity.ShowDetails.ViewModel)
}

class DetailCityViewController: UIViewController {
    
    // MARK: - Outlets
    private var tableView: UITableView = UITableView(frame: .zero, style: .plain)
    private var returnButton: CustomButton = {
        let button = CustomButton(widthAndHeight: 50, image: #imageLiteral(resourceName: "return"))
        button.layer.opacity = 0.7
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return button
    }()
    var currenWeatherView: CurrentView = {
        let view = CurrentView()
        return view
    }()
    
    // MARK: - Properties
    private var detailWeather: [DisplayedDetailWeather] = []
    
    var interactor: DetailCityBusinessLogic?
    var router: (NSObjectProtocol & DetailCityRoutingLogic & DetailCityDataPassing)?
    
    // MARK: - Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showDetails()
        setupUI()
        setup()
    }
    
    func showDetails() {
        interactor?.showDetails()
    }
    
    // MARK: - Configuration UI
    private func setupUI() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        view.addSubview(currenWeatherView)
        currenWeatherView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                                  leading: view.leadingAnchor,
                                  bottom: nil,
                                  trailing: view.trailingAnchor)
        tableView.anchor(top: currenWeatherView.bottomAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.showsVerticalScrollIndicator = false
        
        tableView.register(DetailTableViewCell.self, forCellReuseIdentifier: DetailTableViewCell.reuseId)
        
        view.addSubview(returnButton)
        returnButton.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                            leading: view.leadingAnchor,
                            bottom: nil,
                            trailing: nil,
                            padding: .init(top: 20, left: 16, bottom: 0, right: 0))
    }
    
    // MARK: - Setup
    private func setup() {
        let viewController = self
        let interactor = DetailCityInteractor()
        let presenter = DetailCityPresenter()
        let router = DetailCityRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
}

//MARK: - Actions
extension DetailCityViewController {
    @objc private func handleDismiss() {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITableViewDataSource
extension DetailCityViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailWeather.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailTableViewCell.reuseId,
                                                 for: indexPath) as! DetailTableViewCell
        let city = detailWeather[indexPath.row]
        cell.configure(with: city)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension DetailCityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - DetailCityDisplayLogic
extension DetailCityViewController: DetailCityDisplayLogic {
    
    func displayDetailsWeather(viewModel: DetailCity.ShowDetails.ViewModel) {
        currenWeatherView.configureData(cityWeather: viewModel.myCity)
        self.detailWeather = viewModel.displayedMyCityWeather
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
