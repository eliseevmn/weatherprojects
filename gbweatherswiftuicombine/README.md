Выполнен проект погодного приложения, API - api.openweathermap.org
Проект написан на SwiftUI + Combine
Архитектура приложения - MVVM.
Не использованы сторонние библиотеки.
Работа с сетью с помощью URLSession.
