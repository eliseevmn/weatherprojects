import SwiftUI

struct SearchView: View {

    // MARK: - Propertries
    @Binding var searchTerm: String

    // MARK: - Body
    var body: some View {
        HStack {
            Spacer()
            Image(systemName: "magnifyingglass")

            TextField("Search...", text: self.$searchTerm)
                .font(Font.system(size: 24))
                .foregroundColor(Color.primary)
                .padding(10)

            Spacer()
        } // HStack
        .foregroundColor(.secondary)
        .background(Color(.secondarySystemBackground))
        .cornerRadius(10)
        .padding(20)
    } // Body
}
