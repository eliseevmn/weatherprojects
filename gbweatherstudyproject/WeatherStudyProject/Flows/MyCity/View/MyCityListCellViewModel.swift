import Foundation

class MyCityListCellViewModel: MyCityListCellViewProtocol {

    private let myCity: MyCity

    var cityName: String? {
        return myCity.name
    }

    var currentTime: String? {
        let timeInterval = Double(myCity.currentWeather?.date ?? 0)
        return dateFormatter.string(from: Date(timeIntervalSince1970: timeInterval))
    }

    var currentTemp: String? {
        let tempFromJson = myCity.currentWeather?.main.temp ?? 0.0
        let tempCelcium = String(format: "%.0f℃", tempFromJson - 273.15)
        return tempCelcium
    }

    var weatherIcon: String? {
        let weatherIcon = myCity.currentWeather?.weather[0].icon
        return weatherIcon
    }

    private var dateFormatter: DateFormatter = {
        let date = DateFormatter()
        date.dateFormat = "HH:mm"
        return date
    }()

    required init(city: MyCity) {
        self.myCity = city
    }
}
