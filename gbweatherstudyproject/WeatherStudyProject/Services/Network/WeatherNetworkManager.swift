import Foundation
import Moya

enum WeatherNetworkManager {
    case currentWeather(cityName: String)
    case fiveDailyWeather(cityName: String)
}

extension WeatherNetworkManager: TargetType {

    var baseURL: URL {
        return URL(string: "https://api.openweathermap.org")!
    }

    var path: String {
        switch self {
        case .currentWeather:
            return "/data/2.5/weather"
        case .fiveDailyWeather:
            return "/data/2.5/forecast"
        }
    }

    var method: Moya.Method {
        switch self {
        case .currentWeather:
            return .get
        case .fiveDailyWeather:
            return .get
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .currentWeather(let cityName):
            return .requestParameters(parameters: ["q": cityName,
                                                   "appid": NetworkProperties.apiKey],
                                      encoding: URLEncoding.default)
        case .fiveDailyWeather(cityName: let cityName):
            return .requestParameters(parameters: ["q": cityName,
                                                   "appid": NetworkProperties.apiKey],
                                      encoding: URLEncoding.default)
        }
    }

    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
