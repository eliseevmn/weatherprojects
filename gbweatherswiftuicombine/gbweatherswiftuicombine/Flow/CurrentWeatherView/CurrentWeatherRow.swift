import SwiftUI

struct CurrentWeatherRow: View {

    // MARK: - Properties
    private let viewModel: CurrentWeatherRowViewModel

    // MARK: - Init
    init(viewModel: CurrentWeatherRowViewModel) {
        self.viewModel = viewModel
    }

    // MARK: - Body
    var body: some View {
        VStack(alignment: .leading) {
            MapView(coordinate: viewModel.coordinate)
                .cornerRadius(25)
                .frame(height: 300)
                .disabled(true)

            VStack(alignment: .leading) {
                HStack {
                    Text("☀️ Temperature:")
                    Text("\(viewModel.temperature)°")
                        .foregroundColor(.gray)
                } // HStack

                HStack {
                    Text("📈 Max temperature:")
                    Text("\(viewModel.maxTemperature)°")
                        .foregroundColor(.gray)
                } // HStack

                HStack {
                    Text("📉 Min temperature:")
                    Text("\(viewModel.minTemperature)°")
                        .foregroundColor(.gray)
                } // HStack

                HStack {
                    Text("💧 Humidity:")
                    Text(viewModel.humidity)
                        .foregroundColor(.gray)
                } // HStack
            } // VStack
        } // VStack
    } // Body
}
