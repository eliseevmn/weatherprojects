import UIKit

class MyCityListWorker {

    private var dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "HH:mm"
        return dt
    }()
    
    func getDisplayedCities(from cities: [MyCity]) -> [DisplayedCity] {
        var displayedCities: [DisplayedCity] = []
        
        cities.forEach { (city) in
            let cityName = city.name
            let tempFromNetwork = fetchCurrentTempCelsium(from: city)
            let currentTime = fetchCurrentTime(from: city)
            let iconWeather = city.currentWeather?.weather[0].icon
            
            let displayedCourse = DisplayedCity(cityName: cityName, currentTime: currentTime, currentTemp: tempFromNetwork, iconWeather: iconWeather)
            displayedCities.append(displayedCourse)
        }
        return displayedCities
    }
    
    private func fetchCurrentTempCelsium(from city: MyCity) -> String {
        let tempFromNetwork = city.currentWeather?.main.temp ?? 0.0
        return String(format:"%.0f℃", tempFromNetwork - 273.15)
    }
    
    private func fetchCurrentTime(from city: MyCity) -> String {
        let timeInterval = Double(city.currentWeather?.dt ?? 0)
        return dateFormatter.string(from: Date(timeIntervalSince1970: timeInterval))
    }
}
