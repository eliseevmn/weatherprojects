import Foundation

class ApplicationCoordinator: BaseCoordinator {

    override func start() {
        showMyList()
    }

    private func showMyList() {
        let coordinator = MyListCoordinator()
        coordinator.onFinishFlow = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
            self?.start()
        }
        addDependency(coordinator)
        coordinator.start()
    }
}
