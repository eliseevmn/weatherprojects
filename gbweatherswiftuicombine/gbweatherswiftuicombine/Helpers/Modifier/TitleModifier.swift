//
//  TitleModifier.swift
//  Honeymoon
//
//  Created by MAC on 28.07.2020.
//  Copyright © 2020 MAC. All rights reserved.
//

import SwiftUI

struct TitleModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle)
            .foregroundColor(Color.pink)
    }
}
