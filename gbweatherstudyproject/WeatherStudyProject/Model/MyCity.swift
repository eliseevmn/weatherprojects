import Foundation

// MARK: - MyCity
struct MyCity {
    var name: String
    var dailyWeather: DailyWeatherResponse?
    var currentWeather: CurrentWeatherResponse?

    init(name: String,
         dailyWeather: DailyWeatherResponse?,
         currentWeather: CurrentWeatherResponse?) {
        self.name = name
        self.dailyWeather = dailyWeather
        self.currentWeather = currentWeather
    }
}
