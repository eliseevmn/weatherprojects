import Foundation

protocol AddCityViewModelProtocol {
    var cities: Box<[CityResponse]> { get }
    func fetchCity(searchCity: String, completion: @escaping() -> Void)
    func numberOfRows() -> Int
    func cellViewModel(for indexPath: IndexPath) -> AddCityCellViewModelProtocol?
    func selectRow(for indexPath: IndexPath)
    func viewModelForSelectedRow() -> CityResponse?
}
