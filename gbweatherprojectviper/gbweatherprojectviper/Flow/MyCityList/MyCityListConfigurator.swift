import UIKit

protocol MyCityListConfiguratorProtocol: class {
    func configure(with viewController: MyCityListViewController)
}

class MyCityListConfigurator: MyCityListConfiguratorProtocol {
    func configure(with viewController: MyCityListViewController) {
        let presenter = MyCityListPresenter(view: viewController)
        let interactor = MyCityListInteractor(presenter: presenter)
        let router = MyCityListRouter()
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController
        interactor.presenter = presenter
    }
}
