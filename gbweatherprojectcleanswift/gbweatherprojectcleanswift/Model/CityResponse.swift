import Foundation

struct CityResponse: Decodable {
    let name: String
    
    init(name: String) {
        self.name = name
    }
}
