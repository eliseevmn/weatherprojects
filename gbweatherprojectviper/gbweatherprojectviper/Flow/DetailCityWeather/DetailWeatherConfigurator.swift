import Foundation

protocol DetailWeatherConfiguratorProtocol: class {
    func configure(with viewController: DetailWeatherViewController, and city: MyCity)
}

class DetailWeatherConfigurator: DetailWeatherConfiguratorProtocol {
    func configure(with viewController: DetailWeatherViewController, and city: MyCity) {
        
        let presenter = DetailWeatherPresenter(view: viewController)
        let interactor = DetailWeatherInteractor(presentor: presenter, city: city)
        let router = DetailWeatherRouter()
        
        viewController.presentor = presenter
        presenter.router = router
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.interactor.myCity = city
    }
}
