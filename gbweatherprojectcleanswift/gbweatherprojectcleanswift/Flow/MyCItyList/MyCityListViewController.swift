import UIKit

protocol MyCityListDisplayLogic: class {
    func displayCitiesList(viewModel: MyCityList.FetchCities.ViewModel)
}

class MyCityListViewController: UIViewController {
    
    // MARK: - Outlets
    var tableView = UITableView(frame: .zero, style: .plain)
    private var refreshControler: CustomRefreshController = {
        let refreshControler = CustomRefreshController(frame: .zero)
        refreshControler.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        return refreshControler
    }()
    private var addCityButton: CustomButton = {
        let button = CustomButton(widthAndHeight: 70, image: #imageLiteral(resourceName: "addCity"))
        button.addTarget(self, action: #selector(handleAddCity), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    private var cities: [DisplayedCity] = []
    
    var interactor: MyCityListBusinessLogic?
    var router: (NSObjectProtocol & MyCityListRoutingLogic & MyCityListDataPassing)?
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
        fetchStartCity()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCities()
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Show cities list
    func fetchStartCity() {
        interactor?.fetchCities(cityName: "London")
    }
    
    func fetchCities() {
        interactor?.fetchCities(cityName: router?.dataStore?.searchCity?.name ?? "")
    }
    
    // MARK: Setup
    private func setup() {
        let viewController = self
        let interactor = MyCityListInteractor()
        let presenter = MyCityListPresenter()
        let router = MyCityListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: - Private functions
    private func setupUI() {
        tableView.addSubview(refreshControler)
        view.addSubview(tableView)
        tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .black
        
        tableView.register(MyCityListTableViewCell.self, forCellReuseIdentifier: MyCityListTableViewCell.reuseId)
        
        view.addSubview(addCityButton)
        addCityButton.anchor(top: nil,
                             leading: nil,
                             bottom: view.safeAreaLayoutGuide.bottomAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 0, left: 0, bottom: 40, right: 16))
    }
    
    // MARK: - Actions
    @objc private func handleAddCity() {
        router?.routeToAddCityController()
    }
    
    @objc private func handleRefresh(sender: UIRefreshControl) {
        cities.forEach { (city) in
            interactor?.fetchCities(cityName: city.cityName!)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        sender.endRefreshing()
    }
}

// MARK: - UITableViewDataSource
extension MyCityListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyCityListTableViewCell.reuseId,
                                                 for: indexPath) as! MyCityListTableViewCell
        let city = cities[indexPath.row]
        cell.configure(with: city)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MyCityListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        router?.routeToCityDetails(indexPath: indexPath)
    }
}

// MARK: - MyCityListDisplayLogic
extension MyCityListViewController: MyCityListDisplayLogic {
    func displayCitiesList(viewModel: MyCityList.FetchCities.ViewModel) {
        cities = viewModel.displayedCities
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
