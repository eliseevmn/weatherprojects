import UIKit

protocol MyCityListRouterProtocol {
    func pushToDetailController(on view: MyCityListViewProtocol, city: MyCity)
    func pushToAddCityController(on view: MyCityListViewProtocol)
}

class MyCityListRouter: MyCityListRouterProtocol {
    
    static func createModule() -> UINavigationController {
        let viewController = MyCityListViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        let configurator: MyCityListConfiguratorProtocol = MyCityListConfigurator()
        configurator.configure(with: viewController)
        return navigationController
    }
    
    func pushToDetailController(on view: MyCityListViewProtocol, city: MyCity) {
        print("QuotesRouter is instructed to push QuoteDetailViewController onto the navigation stack.")
        let detailModule = DetailWeatherRouter.createModule(with: city)
        let viewController = view as! MyCityListViewController
        viewController.navigationController?.pushViewController(detailModule, animated: true)
    }
    
    func pushToAddCityController(on view: MyCityListViewProtocol) {
        print("QuotesRouter is instructed to push QuoteDetailViewController onto the navigation stack.")
        guard let delegate = view.presenter as? MyCityListPresenter else {
            return
        }
        
        let detailModule = AddCityRouter.createModule(with: delegate)
        let viewController = view as! MyCityListViewController
        viewController.navigationController?.pushViewController(detailModule, animated: true)
    }
}
