import SwiftUI
import Combine

class CurrentWeatherViewModel: ObservableObject, Identifiable {

    // MARK: - Properties
    @Published var dataSource: CurrentWeatherRowViewModel?

    let city: String
    private let weatherFetcher: WeatherFetcherProtocol
    private var disposables = Set<AnyCancellable>()

    // MARK: - Init
    init(city: String, weatherFetcher: WeatherFetcherProtocol) {
        self.weatherFetcher = weatherFetcher
        self.city = city
    }

    // MARK: - Network functions
    func refresh() {
        weatherFetcher
            .curentWeatherForecast(forCity: city)
            .map(CurrentWeatherRowViewModel.init)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] value in
                guard let self = self else { return }
                switch value {
                case .failure:
                    self.dataSource = nil
                case .finished:
                    break
                }
                }, receiveValue: { [weak self] weather in
                    guard let self = self else { return }
                    self.dataSource = weather
            })
            .store(in: &disposables)
    }
}
