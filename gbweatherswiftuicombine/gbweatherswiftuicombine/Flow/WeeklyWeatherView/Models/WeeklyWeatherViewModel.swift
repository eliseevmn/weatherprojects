import SwiftUI
import Combine

final class WeeklyWeatherViewModel: ObservableObject {

    // MARK: - Properties
    @Published var city: String = ""
    @Published var dataSource: [WeeklyWeatherRowViewModel] = []
    private let weatherFetcher: WeatherFetcherProtocol
    private var disposables = Set<AnyCancellable>()

    // MARK: - Init
    init(weatherFetcher: WeatherFetcherProtocol,
         scheduler: DispatchQueue = DispatchQueue(label: "WeatherViewModel")) {
        self.weatherFetcher = weatherFetcher

        $city
            .dropFirst(1)
            .debounce(for: .seconds(0.5), scheduler: scheduler)
            .sink(receiveValue: fetchWeather(forCity:))
            .store(in: &disposables)
    }

    // MARK: - Network functions
    func fetchWeather(forCity city: String) {
        weatherFetcher.weeklyWeatherForecast(forCity: city)
            .map { response in
                response.list.map(WeeklyWeatherRowViewModel.init)
            }
            .removeDuplicates()
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] value in
                    guard let self = self else { return }
                    switch value {
                    case .failure:
                        self.dataSource = []
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] forecast in
                    guard let self = self else { return }
                    self.dataSource = forecast
            })
            .store(in: &disposables)
    }

    // MARK: - View builder
    var currentWeatherView: some View {
        return WeeklyWeatherBulder.makeCurrentWeatherView(
            withCity: city,
            weatherFetcher: weatherFetcher)
    }
}
